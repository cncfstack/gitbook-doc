# 模块及功能预览

模块名称                                | 功能描述 
---                                     | --- 
[accordion](accordion.md)               |  手风琴折叠模块
[ace](cat-code.md)       | 代码 ACE 显示
[advanced-emoji](cat-emoji.md)                  | 显示 emoji 表情
[alerts](cat-words-emphasize.md)                | 告警级别信息提示
[anchor-navigation-ex](cat-article-content.md ) | 悬浮目录和回到顶部
[ancre-navigation](cat-article-content.md)      | 悬浮目录和回到顶部
[auto-scroll-table](cat-table.md)               | 表格滚动条
[back-to-top-button](cat-article-content.md)    | 当页面超过一屏幕时，会显示一个 回到顶部按钮
[change_girls](cat-background.md)               | 可自动切换的背景
[click-reveal](cat-text-hide.m)                 | 默认隐藏，点击可显示
[code](cat-code.md)                             | 代码添加行号&复制按钮
[copy-code-button](cat-code.md)         | 代码复制按钮
[custom-favicon](cat-logo.md)             | 修改网页标题图标 favicon 
[chapter-fold](cat-side-nav.md)                 | 导航目录默认折叠
[Chart](cat-chart.md)                       | 绘制图形
[donate](cat-donate.md)                      | 贡献
[disqus](cat-disqus.md)                       | 评论系统
[emphasize](cat-words-emphasize.md)             | 为文字加上底色 
[expandable-chapters](cat-side-nav.md)          | 导航目录折叠扩展 
[expandable-chapters-small](cat-side-nav.md)    | expandable-chapters-small
[edit-link](cat-edit-link.md)                   | 添加编辑按钮
[favicon](cat-logo.md)                          | 显示网站图标
[flexible-alerts](cat-words-emphasize.md)       | flexible-alerts
[github](cat-github.md)                         | 在右上角添加 github 图标 
[hide-element](cat-text-hide.md)                | 可以隐藏不想看到的元素，比如导航栏中 Published by GitBook 
[insert-logo](cat-logo.md)                      | 插入logo
[include-codeblock](cat-include.md)             | 包含或显示文件
[include-csv](cat-include.md)                   | 显示 CSV 文件内容
[KaTex](cat-math.md)                            | 数学公式支持
[klipse](cat-logo.md)                             | klipse 嵌入类似IDE的功能
[lightbox](cat-image.md)                            | 单击查看图片 点击图片可显示，大小不变 
[lunr](lunr.md)                                 | lunr
[local-video](cat-video.md)   | 视频
[mermaid](cat-chart.md)                           | 流程图
[Mermaid-gb3](cat-chart.md)                      | 流程图
[Musicxml](cat-chart.md)       |  乐谱渲染
[page-copyright](cat-page-copyright.md)             | 页面页脚版权
[page-toc-button](cat-article-content.md )           | 悬浮目录
[page-top](cat-article-content.md)                         | page-top.md
[page-treeview](cat-article-content.md)               | 生成页内目录
[page-treeview-simle](cat-article-content.md)               | 生成页内目录精简版本
[pageview-count](pageview-count.md)             | 阅读量计数
[popup](cat-image.md)                               | 单击图片，在新页面查看大图
[prism](cat-code.md)                               |  语法高亮
[PlantUML](cat-chart.md)                           | UML 
[reward](cat-donate.md)                             | 赞赏组件
[rss](cat-rss.md)  | RSS  订阅
[search](cat-search.md)                         | 搜索
[search-pro](cat-search.md)                     | 高级搜索
[search-plus](cat-search.md)                    | search-plus
[sectionx.md](cat-text-hide.md)                      | sectionx
[simple-page-toc(cat-article-content.md)            | plugins/simple-page-toc
[sharing-plus](cat-share.md)                    | 分享当前页面 
[sidebar-style](cat-side-nav.md)               | 会替换掉 Published by GitBook
[splitter](splitter.md)                         | 侧边栏宽度可调节
[sitemap-general](cat-seo.md)               | 自动生成 sitemap 文件
[summary](cat-summary.md)                   | 自动生成 SUMMARY.md
[tbfed-pagefooter](cat-page-copyright.md)         | 页面添加页脚
[tags](tags.md)                                 | tags
[todo](cat-todo.md)                                 | todo
[Terminal](cat-code.md)        | Terminal 终端
[Version-select](cat-version-select.md) |文档多版本



注意： GitBook 默认自带5个插件

- highlight： 代码高亮
- search： 导航栏查询功能（不支持中文）
- sharing：右上角分享功能
- font-settings：字体设置（最上方的"A"符号）
- livereload：为GitBook实时重新加载


