# TODO 代办


## todo 代办 

- 插件地址：<https://www.npmjs.com/package/gitbook-plugin-todo>

> **[!WARNING|style:flat] **
> 安装时失败 
> `install plugin "todo" (*) from NPM with version 0.1.3`

### 使用方法

```
{
    "plugins": ["todo"]
}

```

### 示例


Markdown 语法

```
* [ ]  write some articles
* [x]  drink a cup of tea
```

渲染效果

* [ ]  write some articles
* [x]  drink a cup of tea
