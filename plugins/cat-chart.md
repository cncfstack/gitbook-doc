# 图与图表

## 流程图（mermaid）

- 插件地址：<https://github.com/JozoVilcek/gitbook-plugin-mermaid>


### 使用方法

- 添加配置 book.json

```
{
    "plugins": ["mermaid"]
}
```

### 示例

```
{% mermaid %}
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
{% endmermaid %}
```


## Mermaid-gb3

支持渲染Mermaid图表

- 插件地址: <https://www.npmjs.com/package/gitbook-plugin-mermaid-gb3>

```
"plugins": [
    "mermaid-gb3"
]
```



## Puml

使用 PlantUML 展示 uml 图。

- 插件地址: <https://www.npmjs.com/package/gitbook-plugin-puml>


```
{
    "plugins": ["puml"]
}
```

使用示例：

```
{% plantuml %}
Class Stage
    Class Timeout {
        +constructor:function(cfg)
        +timeout:function(ctx)
        +overdue:function(ctx)
        +stage: Stage
    }
    Stage <|-- Timeout
{% endplantuml %}
```

效果如下所示：

{% plantuml %}
Class Stage
    Class Timeout {
        +constructor:function(cfg)
        +timeout:function(ctx)
        +overdue:function(ctx)
        +stage: Stage
    }
    Stage <|-- Timeout
{% endplantuml %}

## Graph

使用 function-plot 绘制数学函数图。

插件地址

function-plot

```
{
    "plugins": [ "graph" ],
}
```

下面是一个示例，需要注意的是 `{% graph %}` 块中的内容必须是合法的 JSON 格式。

```
{% graph %}
{
    "title": "1/x * cos(1/x)",
    "grid": true,
    "xAxis": {
        "domain": [0.01, 1]
    },
    "yAxis": {
        "domain": [-100, 100]
    },
    "data": [{
        "fn": "1/x * cos(1/x)",
        "closed": true
    }]
}
{% endgraph %}
```

效果如下所示：

{% graph %}
{
    "title": "1/x * cos(1/x)",
    "grid": true,
    "xAxis": {
        "domain": [0.01, 1]
    },
    "yAxis": {
        "domain": [-100, 100]
    },
    "data": [{
        "fn": "1/x * cos(1/x)",
        "closed": true
    }]
}
{% endgraph %}


## Chart

使用 C3.js 或者 Highcharts 绘制图形。

- 插件地址: <https://plugins.gitbook.com/plugin/chart>
- C3.js: <https://github.com/c3js/c3>
- highcharts: <https://github.com/highcharts/highcharts>

```
{
    "plugins": [ "chart" ],
    "pluginsConfig": {
        "chart": {
            "type": "c3"
        }
    }
}
```

type 可以是 c3 或者 highcharts, 默认是 c3.

下面是一个示例：

```
{% chart %}
{
    "data": {
        "type": "bar",
        "columns": [
            ["data1", 30, 200, 100, 400, 150, 250],
            ["data2", 50, 20, 10, 40, 15, 25]
        ],
        "axes": {
            "data2": "y2"
        }
    },
    "axis": {
        "y2": {
            "show": true
        }
    }
}
{% endchart %}
```

{% chart %}
{
    "data": {
        "type": "bar",
        "columns": [
            ["data1", 30, 200, 100, 400, 150, 250],
            ["data2", 50, 20, 10, 40, 15, 25]
        ],
        "axes": {
            "data2": "y2"
        }
    },
    "axis": {
        "y2": {
            "show": true
        }
    }
}
{% endchart %}


## Musicxml

支持 musicxml 格式的乐谱渲染。

插件地址
musicXML

```
{
    "plugins": ["musicxml"]
}
```

下面是一个示例，需要注意的是 block 中的内容必须是一个合法的 musicxml 文件路径，并且不能有换行和空格。

```
{% musicxml %}assets/musicxml/mandoline - debussy.xml{% endmusicxml %}
```

效果如下所示
