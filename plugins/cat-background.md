# 背景设置


## change_girls 插件

设置背景，并可自动切换的背景图片

### 使用方法

```
{
    "plugins":["change_girls"],
    "pluginsConfig": {
        "change_girls" : {
            "time" : 10,
            "urls" : [
                "girlUrl1", "girlUrl2",...""
            ]
        }
    }
}
```

### 示例

关注当前页面的背景，等待 10 秒观察背景变化

```
  "pluginsConfig": {
    "change_girls" : {
        "time" : 10,
        "urls" : [
            "https://www.zhaowenyu.com/assets/imgs/banner/shan.jpg", "https://www.zhaowenyu.com/assets/imgs/banner/shui.jpg"
        ]
    },
```


