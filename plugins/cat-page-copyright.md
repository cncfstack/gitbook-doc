# 版权声明

站点底部的版权相关说明配置。


## tbfed-pagefooter 插件

页面添加页脚

### 使用方法

```
{
    "plugins": [
       "tbfed-pagefooter"
    ],
    "pluginsConfig": {
        "tbfed-pagefooter": {
            "copyright":"Copyright &copy learn.fuming.site 2019",
            "modify_label": "该文件修订时间：",
            "modify_format": "YYYY-MM-DD HH:mm:ss"
        }
    }
}
```

### 示例

见当前页面的页脚

![tbfed-pagefooter](/assets/imgs/tbfed-pagefooter.png)


## page-copyright 插件

页面页脚版权（内容更多）

### 使用方法

```
{
    "plugins" : ["page-copyright"],
    "pluginsConfig" : {
        "page-copyright": {
          "description": "modified at",
          "signature": "付铭",
          "wisdom": "前端开发者 & 全栈工程师",
          "format": "YYYY-MM-dd hh:mm:ss",
          "copyright": "Copyright &#169; 铭哥教程",
          "timeColor": "#666",
          "copyrightColor": "#666",
          "utcOffset": "8",
          "style": "normal",
          "noPowered": false,
        }
    }
}
```

运行以后有很多信息是原创作者的，这些配置都在你的插件安装目录 `**\node_modules\gitbook-plugin-page-copyright` 下的 index.js 中，自己可以修改。大部分信息都在 defaultOption 中。

那个二维码可以在文件中找到 QRcode 改成自己的，或者直接把所有的 efaultOption.isShowQRCode 改成false

### 示例

