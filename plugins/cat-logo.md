# 图标与LOGO

## custom-favicon 插件

修改网页标题图标 favicon

> **[!NOTE|style:flat] 注意**
>
> 这个 `pluginsConfig` 和其他的不大一样。图标只能用 `.ico` 文件。

### 使用方法

```
{
    "plugins" : ["custom-favicon"],
    "pluginsConfig" : {
        "favicon": "path/to/favicon.ico"
    }
}
```


把`.ico` 格式的图标放进项目中。这个路径可以使用相对路径，比如`./images/a.ico`


### 示例



## favicon 

修改网站图标


### 使用方法

```
{
    "plugins": [
        "favicon"
    ],
    "pluginsConfig": {
        "favicon": {
            "shortcut": "assets/images/favicon.ico",
            "bookmark": "assets/images/favicon.ico",
            "appleTouch": "assets/images/apple-touch-icon.png",
            "appleTouchMore": {
                "120x120": "assets/images/apple-touch-icon-120x120.png",
                "180x180": "assets/images/apple-touch-icon-180x180.png"
            }
        }
    }
}
```





## insert-logo

insert-logo 插入logo

### 使用方法

```
{
    "plugins": [ "insert-logo" ]
    "pluginsConfig": {
      "insert-logo": {
        "url": "http://www.fuming.site/dist/avator.jpg",
        "style": "background: none; max-height: 30px; min-height: 30px"
      }
    }
}
```

### 示例

效果图

![insert-logo](/assets/imgs/insert-logo.png)
