<!-- toc -->

# 文章目录导航

文章目录导航插件使文章的阅读更方便。


## anchor-navigation-ex 插件

悬浮目录和回到顶部

添加 Toc 到侧边悬浮导航以及回到顶部按钮。需要注意以下两点：

本插件只会提取 h[1-3] 标签作为悬浮导航

只有按照以下顺序嵌套才会被提取

> [!NOTE|style:flat]
>
> 1. 必须要以 h1 开始，直接写 h2 不会被提取
> 2. `anchor-navigation-ex` 和  会互相叠加影响，应选择其中一种即可



### 使用说明

在 books.json 添加配置

```
{
    plugins: ["anchor-navigation-ex"],
    pluginsConfig: [
        "anchor-navigation-ex": {
            "tocLevel1Icon": "fa fa-hand-o-right",
            "tocLevel2Icon": "fa fa-hand-o-right",
            "tocLevel3Icon": "fa fa-hand-o-right",
            "multipleH1": false,
            "multipleH2": false,
            "multipleH3": false,
            "multipleH4": false,
            "showLevelIcon": false,
            "showLevel": false
        },
}
```

在文章中使用1/2/3级标题

```
# h1
## h2
### h3
```

### 示例

右上角目录图标预览效果


![anchor-navigation-ex](/assets/imgs/anchor-navigation-ex2.png)



-------------
## ancre-navigation 插件

右上角悬浮导航和回到顶部按钮


### 使用方法


```
{
    plugins: ["ancre-navigation"]
}
```

### 示例

![ancre-navigation](/assets/imgs/ancre-navigation.png)



-------------
## back-to-top-button 插件

当页面超过一屏幕时，会显示一个 回到顶部按钮

地址：
- https://github.com/stuebersystems/gitbook-plugin-back-to-top-button


### books.json 配置
```
{
    "plugins": [
         "back-to-top-button"
    ]
}
```



### 示例

效果图,注意关注页面的右下脚图标

![回到顶部按钮](/assets/imgs/back-to-top-button.png)





## page-toc-button 插件

悬浮目录

### 使用方法


```
{
    "plugins" : [ "page-toc-button" ]
}
```

可选配置项：

```
"pluginsConfig": {
        "page-toc-button": {
            "maxTocDepth": 2,
            "minTocSize": 2
       }
}
```

maxTocDept 标题的最大深度（2 = h1 + h2 + h3）。不支持值> 2。 默认2

minTocSize 显示toc按钮的最小toc条目数。 默认 2


### 示例

![page-toc-button](/assets/imgs/page-toc-button1.png)

![page-toc-button](/assets/imgs/page-toc-button2.png)

