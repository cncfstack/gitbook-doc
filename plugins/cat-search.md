# 查询

搜索查询

## search
查找内容, 不支持中文

- 插件地址：<https://www.npmjs.com/package/gitbook-plugin-search>


## search-pro 高级搜索

至此好中文搜索

> **[!TIP|style:flat]**
> 在搜索结果中，关键字会高亮；自带的 search 插件，关键字不会高亮

- 插件地址： <https://www.npmjs.com/package/gitbook-plugin-search-pro>

> **[!NOTE|style:flat]**
> 在使用此插件之前，需要将默认的 `search` 和 `lunr` 插件去掉；



### 使用方法

```
{
    "plugins": [
          "-lunr", 
          "-search", 
          "search-pro"
    ]
}
```

### 示例

见当前 book 左上角查找框 `输入并搜索`

![search-pro](/assets/imgs/search-pro.png)


## search-plus 插件

- 插件地址：<https://www.npmjs.com/package/gitbook-plugin-search-plus>
