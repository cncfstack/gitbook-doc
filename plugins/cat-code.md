# 代码块


- [code](code.md) 插件支持复制按钮，也支持代码行号
- [copy-code-button](copy-code-button.md) 插件只支持复制按钮功

## code 插件

code 代码添加行号&复制按钮


### 使用说明

```
{
    "plugins" : [ "code" ]
}
```

如果想去掉复制按钮，可配置

```
"pluginsConfig": {
      "code": {
        "copyButtons": false
      }
}
```

### 示例
效果图如下：

![code](/assets/imgs/code.png)

注意左侧行数编号和右上角的复制按钮

---

## copy-code-button

代码复制按钮


### 使用方法

```
{
    "plugins": ["copy-code-button"]
}
```

### 示例

效果图如下：

![copy-code-button](/assets/imgs/copy-code-button.png)




## klipse


klipse 嵌入类似IDE的功能

### 使用方法

```
{
    "plugins": ["klipse"]
}
```

用法，在 markdown 中


<pre>
```eval-python
print [x + 1 for x in range(10)]
```
</pre>

效果如下：

```eval-python
print [x + 1 for x in range(10)]
```


klipse 目前支持下面的语言：

- javascript: evaluation is done with the javascript function eval and pretty printing of the result is done with pretty-format
- clojure[script]: evaluation is done with Self-Hosted Clojurescript
- ruby: evaluation is done with Opal
- C++: evaluation is done with JSCPP
- python: evaluation is done with Skulpt
- scheme: evaluation is done with BiwasScheme
- PHP: evaluation is done with Uniter
- BrainFuck
- JSX
- EcmaScript2017
- Google Charts: See Interactive Business Report with Google Charts.


## Prism 语法高亮

使用 Prism.js 为语法添加高亮显示，需要将 highlight 插件去掉。该插件自带的主题样式较少，可以再安装 prism-themes 插件，里面多提供了几种样式，具体的样式可以参考 这里，在设置样式时要注意设置 css 文件名，而不是样式名。

- 插件地址: <https://www.npmjs.com/package/gitbook-plugin-prism>

### 使用方法

```
{
    "plugins": [
        "prism",
        "-highlight"
    ],
    "pluginsConfig": {
        "prism": {
            "css": [
                "prism-themes/themes/prism-base16-ateliersulphurpool.light.css"
            ]
        }
    }
}
```

如果需要修改背景色、字体大小等，可以在 website.css 定义 pre[class*="language-"] 类来修改，下面是一个示例：

### 示例

```
pre[class*="language-"] {
    border: none;
    background-color: #f7f7f7;
    font-size: 1em;
    line-height: 1.2em;
}
```


## ACE 

使 GitBook 支持ace 。默认情况下，line-height 为 1，会使代码显得比较挤，而作者好像没提供修改行高的选项，如果需要修改行高，可以到 `node_modules -> github-plugin-ace -> assets -> ace.js` 中加入下面两行代码 (30 行左右的位置)：

```
editor.container.style.lineHeight = 1.25;
editor.renderer.updateFontSize();
```

不过上面的做法有个问题就是，每次使用 gitbook install 安装新的插件之后，代码又会重置为原来的样子。另外可以在 website.css 中加入下面的 css 代码来指定 ace 字体的大小

```
.aceCode {
  font-size: 14px !important;
}
```

使用插件：

```
"plugins": [
    "ace"
]
```

使用示例:


```
// This is a hello world program for C.
#include <stdio.h>

int main(){
  printf("Hello World!");
  return 1;
}
```



## Terminal

模拟终端显示，主要用于显示命令以及多行输出，不过写起来有些麻烦。

插件地址

```
{
    "plugins": [
        "terminal"
    ],
    "pluginsConfig": {
        "terminal": {
            "copyButtons": true,
            "fade": false,
            "style": "flat"
        }
    }
}
```

现在支持 6 种标签：

```
command: Command "executed" in the terminal.
delimiter: Sequence of characters between the prompt and the command.
error: Error message.
path: Directory path shown in the prompt.
prompt: Prompt of the user.
warning: Warning message.
```

标签的使用格式如下所示：

```
**[<tag_name> 内容]
```

为了使标签正常工作，需要在代码块的第一行加入 **[termial] 标记，下面是一个完整的示例：

```
**[terminal]
**[prompt foo@joe]**[path ~]**[delimiter  $ ]**[command ./myscript]
Normal output line. Nothing special here...
But...
You can add some colors. What about a warning message?
**[warning [WARNING] The color depends on the theme. Could look normal too]
What about an error message?
**[error [ERROR] This is not the error you are looking for]
```

效果如下所示：

terminal 支持下面 5 种样式，如果需要更换样式，在 pluginsConfig 里配置即可。

- black: Just that good old black terminal everybody loves.
- classic: Looking for green color font over a black background? This is for you.
- flat: Oh, flat colors. I love flat colors. Everything looks modern with them.
- ubuntu: Admit it or not, but Ubuntu have a good looking terminal.
- white: Make your terminal to blend in with your GitBook.
