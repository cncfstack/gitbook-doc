# 侧边导航

Gitbook 提供的书籍默认都会在左侧提供书籍目录索引导航的功能。

对于侧边导航功能、UI、布局、效果等等有一些开源的插件可供选择，插件的核心功能与差异：

- chapter-fold 插件: 默认的侧边目录是全部展开的，该插件可以让文章按照层级目录折叠起来，同时只能展开一个目录。
- expandable-chapters 插件: 默认的侧边目录是全部展开的，该插件可以让文章按照层级目录折叠起来，展开后不会自动折叠。
- expandable-chapters-small 插件: 默认的侧边目录是全部展开的，该插件可以让文章按照层级目录折叠起来，展开后不会自动折叠，箭头相比 `expandable-chapters` 会细一些。
- sidebar-style 插件:  会替换掉 `Published by GitBook`，并在左侧最上面显示标题。
- splitter 插件: 提供侧边栏宽度可调节功能。

本站点使用：
- expandable-chapters-small 插件
- sidebar-style 插件
- splitter 插件


## chapter-fold 插件

默认的侧边目录是全部展开的，该插件可以使左侧导航目录默认折叠。

在点击折叠目录时，只会展开一个目录，新的目录展开时其他目录会折叠。

### 使用方法

```
{
    "plugins": ["chapter-fold"]
}
```

### 示例

![chapter-fold](/assets/imgs/chapter-fold2.png)



## expandable-chapters 插件

默认的侧边目录是全部展开的，该插件可以使左侧导航目录默认折叠。

可以支持点击展开后的目录一直保持展开的状态，这点和 `chapter-fold` 不同。

和 expandable-chapters-small 效果相同，唯一不同的是这个插件的箭头粗一些。

### 使用方法

```
{
    "plugins": [
         "expandable-chapters"
    ]
}
```

### 示例

![expandable-chapters](/assets/imgs/expandable-chapters.png)


## expandable-chapters-small 插件

默认的侧边目录是全部展开的，该插件可以使左侧导航目录默认折叠。

可以支持点击展开后的目录一直保持展开的状态，这点和 `chapter-fold` 不同。

和  `expandable-chapters 插件` 效果相同，差异是折叠展开箭头图标小一些。

### 使用方法

```
{
    "plugins": [
         "expandable-chapters-small"
    ]
}
```

### 示例

效果图

![expandable-chapters-small](/assets/imgs/expandable-chapters-small2.png)

## sidebar-style  插件

在左侧最上方添加标题。

在左侧导航最下方替换掉 `Published by GitBook` 提示信息


### 使用方法

```
{
    "plugins": ["sidebar-style"],
    "pluginsConfig": {
        "sidebar-style": {
            "title": "《Gitbook 文档》",
            "author": "温玉"
        }
    }
}
```

### 示例

见当前 Book 左下提示

## splitter 插件

侧边栏宽度可调节

### 使用方法

```
{
    "plugins": [
        "splitter"
    ]
}
```

### 示例

![splitter](/assets/imgs/splitter.png)
