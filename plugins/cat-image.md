# 图片查看

- [lightbox](#lightbox-插件): 当前页面打开图片
- [popup](#popup-插件): 新页面打开图片

## lightbox 插件

单击查看图片，以弹窗形式查看图片，查看原图。

> [WARN|style:flat]
> 该插件默认已经不可安装使用


### 使用方法

```
{
  "plugins": ["lightbox"]
}
```

### 示例

![cncfstack](/assets/imgs/cncfstack.jpg)



## popup 插件

单击图片，在新页面查看大图。

### 使用方法

```
{
  "plugins": [ "popup" ]
}
```

### 示例

popup 和 lightbox 冲突，只能保留1个效果。
