# 数学公式


为了支持数学公式, 我们可以使用KaTex和MathJax插件, 官网上说Katex速度要快于MathJax


## katex

MathJax使用LaTeX语法编写数学公式教程

- 插件地址: <https://www.npmjs.com/package/gitbook-plugin-katex>

> **[!WARNING|type:flat]**
> 安装时报错: `unknown block tag: endblock`

```
"plugins": [
    "katex"
]

```

使用示例:

```
Inline math: $$\int_{-\infty}^\infty g(x) dx$$


Block math:

$$
\int_{-\infty}^\infty g(x) dx
$$

Or using the templating syntax:

{% math %}\int_{-\infty}^\infty g(x) dx{% endblock %}
```



