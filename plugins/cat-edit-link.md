# 页面编辑

## edit-link

添加编辑按钮


### 使用方法

```
{
    "plugins": [
         "edit-link"
    ]
}
```

###  示例

![editlink](/assets/imgs/edit-link.png)
