# 文章 TOC 目录 

文章目录导航插件使文章的阅读更方便。




## page-treeview 插件

生成页内目录

不需要插入标签，能支持到6级目录，安装可用

```
{
    "plugins": ["page-treeview"]
}
```

非必要配置：
```
"pluginsConfig": {
    "page-treeview": {
        "copyright": "Copyright &#169; aleen42",
        "minHeaderCount": "2",
        "minHeaderDeep": "2"
    }
}
```

这个插件生成目录以后，下面有一行关于版权的文字。如果想去掉的话，找到插件目录下的index.js文件：`***/node_modules/gitbook-plugin-page-treeview/lib/index.js` 找到如下这段代码


### 示例

![page-treeview.png](/assets/imgs/page-treeview.png)


## page-treeview-simple 插件

和 page-treeview 功能相同，在其基础之上修改了以下内容：

- 去除 copyRight 的提示内容与占用的空白高
- 取消章节的折叠效果，默认展开显示完整章节

### 示例

![page-treeview-simple](/assets/imgs/page-treeview-simple.png)

## simple-page-toc  插件

生成本页目录
需要在文章中插入标签，支持1-3级目录
页面顶端生成。另外 GitBook 在处理重复的标题时有些问题，所以尽量不适用重复的标题。

```
{
    "plugins" : [
        "simple-page-toc"
    ],
    "pluginsConfig": {
        "simple-page-toc": {
            "maxDepth": 3,
            "skipFirstH1": true
        }
    }
}
```

参数|	说明
--|--
"maxDepth": 3|	使用深度最多为maxdepth的标题。
"skipFirstH1": true|	排除文件中的第一个h1级标题。

使用方法: 在需要生成目录的地方用下面的标签括起来，全文都生成的话就在首尾添加

![simple-page-toc-code](/assets/imgs/simple-page-toc-code.png)


### 示例

![simple-page-toc](/assets/imgs/simple-page-toc2.png)


## page-toc 插件
todo
