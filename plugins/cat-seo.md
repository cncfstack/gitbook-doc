# SEO

## sitemap

生成sitemap

- 插件地址：<https://github.com/GitbookIO/plugin-sitemap>

npm 安装插件(或配置到 books.json 中使用 `gitbook install` 安装)：

```
npm install gitbook-plugin-sitemap
```

gitbook 配置

```
{
    "plugins": ["sitemap"],
    "pluginsConfig": {
        "hostname": {
            "prefix": "https://www.example.com"
        }
    }
}
```



## sitemap-general

用来生成网站地图 sitemap (网站中页面的 URL 地址列表)

npm 安装插件(或配置到 books.json 中使用 `gitbook install` 安装)：

```
npm install gitbook-plugin-sitemap-general
```

gitbook 配置

```
{
    "plugins": ["sitemap-general"],
    "pluginsConfig": {
        "sitemap-general": {
            "prefix": "http://gitbook.example.com"
        }
    }
}
```
