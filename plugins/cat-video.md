# 视频
## 使用 HTML 视频标签

可在 Markdown 中使用原生的 HTML 标签 `<video></video>` 来显示视频
```

<video id="video" controls preload="metadata" poster="https://www.zhaowenyu.com/cncfstack-wxdata/wxdata-index-pic.jpg" width="100">
      <source id="mp4" src="https://www.zhaowenyu.com/cncfstack-wxdata/wxdata-start.mp4" type="video/mp4">
</videos>
```

注意这里 poster 是视频最开始预览的一张图，如果没有正常定义会导致视频框只有底部部分内容。

关于 HTML 标签 `<video></video>` 详细说明参考
- https://developer.mozilla.org/zh-CN/docs/Web/HTML/Element/video


## Local Video

使用Video.js 播放本地视频
插件地址

```
"plugins": [ "local-video" ]
```

使用示例：为了使视频可以自适应，我们指定视频的width为100%，并设置宽高比为16:9，如下面所示
```
{% raw %}
<video id="my-video" class="video-js" controls preload="auto" width="100%"
poster="https://zhangjikai.com/resource/poster.jpg" data-setup='{"aspectRatio":"16:9"}'>
  <source src="https://zhangjikai.com/resource/demo.mp4" type='video/mp4' >
  <p class="vjs-no-js">
    To view this video please enable JavaScript, and consider upgrading to a web browser that
    <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
  </p>
</video>
{% endraw %}
```

另外我们还要再配置下css，即在website.css中加入

```
.video-js {
    width:100%;
    height: 100%;
}
```
