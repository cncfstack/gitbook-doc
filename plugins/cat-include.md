# include

## Include Codeblock

- 插件地址：<https://www.npmjs.com/package/gitbook-plugin-include-codeblock>


使用代码块的格式显示所包含文件的内容. 该文件必须存在。插件提供了一些配置，可以区插件官网查看。如果同时使用 ace 和本插件，本插件要在 ace 插件前面加载。

插件地址

```
{
    "plugins": [
        "include-codeblock"
    ],
    "pluginsConfig": {
        "include-codeblock": {
            "template": "ace",
            "unindent": "true",
            "theme": "monokai"
        }
    }
}
```


## include-csv

展示 csv 文件。

插件地址

```
{
    "plugins": ["include-csv"]
}
```

使用示例：
```
{% includeCsv  src="./assets/csv/test.csv", useHeader="true" %} {% endincludeCsv %}
```

效果如下所示：

