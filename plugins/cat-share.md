# 分享

分享页面


## sharing 插件
TODO


## sharing-plus 插件

分享当前页面，比默认的 sharing 插件多了一些分享方式。

### 使用方法

```
{
    "plugins": ["-sharing", "sharing-plus"],
    "pluginsConfig": {
        "sharing": {
             "douban": false,
             "facebook": false,
             "google": true,
             "pocket": false,
             "qq": false,
             "qzone": true,
             "twitter": false,
             "weibo": true,
          "all": [
               "douban", "facebook", "google", "instapaper", "linkedin","twitter", "weibo", 
               "messenger","qq", "qzone","viber","whatsapp"
           ]
       }
    }
}
```

其中：
为true的代表直接显示在页面顶端，为false的不显示，不写默认为false
"all"中代表点击分享符号显示出来的

支持的网站如下：

>"douban", "facebook", "google", "hatenaBookmark", 
"instapaper", "linkedin","twitter", "weibo", 
"messenger","qq", "qzone","viber","vk","weibo",
"pocket", "stumbleupon","whatsapp"



### 示例

见当前 book 右上角分享
