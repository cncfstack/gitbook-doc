# 打赏插件



## donate 打赏插件

### 使用方法

```
{
    plugins: [
        ”donate“
    ],

    "pluginsConfig": ['
        "donate": {
          "wechat": "https://my-pay-code.oss-cn-hangzhou.aliyuncs.com/wechat-pay.jpg",
          "alipay": "https://my-pay-code.oss-cn-hangzhou.aliyuncs.com/ali-pay.jpg",
          "title": "鼓励一下",
          "button": "赏",
          "alipayText": "支付宝打赏",
          "wechatText": "微信打赏"
        },
    ]
}
```

### 示例

![donate](/assets/imgs/donate.png)

----
## reward 底部打上

打赏组件

### 使用方法

```
{
    plugins: [
        ”reward“
    ],

    "pluginsConfig": ['
        "reward": {
            "wechat": "http://unclealan.cn/usr/themes/seventeen/dist/images/weixin.JPG",
            "alipay": "http://unclealan.cn/usr/themes/seventeen/dist/images/alipay.JPG",
            "button": "打赏铭哥",
            "alipayText": "支付宝",
            "wechatText": "微信"
        },
    ]
}
```

### 示例

见页面最底部 `打赏`

