# 表格

## auto-scroll-table

为避免表格过宽，添加滚动条。

在超过文章默认宽度时才会显示，不够宽也是没有滚动条的。

### 使用方法

```
{
  "plugins": ["auto-scroll-table"]
}
```

### 示例子

```
字段1 | 字段2 | 字段3
---|---|----
值1 | 值2abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ12345678901234567892abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890123456789 | 值3
```


字段1 | 字段2 | 字段3
---|---|----
值1 | 值2abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ12345678901234567892abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890123456789 | 值3
