# Gitbook 插件

本站点收集的插件大部分在 GitBook 的 3.2.3 版本中可以正常工作，因为一些插件可能不会随着 GitBook 版本的升级而升级，即下面的插件可能不适用高版本的 GitBook。 

大部分插件都针对默认主题的，如果指定了其他主题插件，可能会导致部分插件失效或显示错乱。

由于 Gitbook 的原创团队重心偏向 gitbook.com 的运行，目前 插件官网 <https://plugins.gitbook.com> 已无法打开。

示例中给出的插件链接换成了 github 的地址


## 默认插件

Gitbook默认带有7个插件（功能性5个，搜索有两个，主题一个）：

- livereload 热加载插件
- highlight 语法高亮插件
- search 搜索插件
- lunr 搜索插件后台服务
- sharing 分享插件
- fontsettings 字体设置插件
- theme-default 主题

## 添加和移除插件

在 book.json 中的 plugins 模块能够配置，能够使 Gitbook 的功能更丰富。

在 plugins 中添加对应的插件名称

```
"plugins": [
    "highlight",
    "-search",
    "back-to-top-button",
    "expandable-chapters-small",
    "insert-logo"
]
```

其中"-search"中的 `-` 符号代表去除默认自带的插件


## 插件属性配置pluginsConfig

部分插件在引入后需要进行详细的配置，可以在 pluginsConfig 中加配置说明。

```
"pluginsConfig": {
    "insert-logo": {
      "url": "images/logo.png",
      "style": "background: none; max-height: 30px; min-height: 30px"
    }
}
```

## 插件安装

**方法一：**

插件无需单独下载安装，只有在 book.json 中配置好对应的插件，执行 `gitbook install`, 缺少的插件就会自动下载安装

```
gitbook install .
```

**方法二：**
自定义的插件也可以通过 npm 或者将解压后的包复制到 node_modules 目录中进行安装。

```
npm install gitbook-plugin-xxx
```

**方法三：**
也可以从源码GitHub地址中下载，放到 node_modules 文件夹里（GitHub地址在进入插件地址右侧的GitHub链接）：
