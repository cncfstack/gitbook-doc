# github

在右上角添加 github 图标。

这里是提供 github 的图标和URL 配置，URL可以根据自己需求配置。

## 使用方法

```
{
    "plugins": [ 
        "github" 
    ],
    "pluginsConfig": {
        "github": {
            "url": "https://github.com/unclealan"
        }
    }
}
```

## 示例

![github](/assets/imgs/github.png)



## Github Buttons

添加项目在 github 上的 star，watch，fork情况

> **[TIP|style:flat]**
> GITHUB 访问慢时会显示异常


- 插件地址: <https://www.npmjs.com/package/gitbook-plugin-github-buttons>


```
{
    "plugins": [
        "github-buttons"
    ],
    "pluginsConfig": {
        "github-buttons": {
            "repo": "zhangjikai/gitbook-use",
            "types": [
                "star",
                "watch",
                "fork"
            ],
            "size": "small"
        }
    }
}
```
