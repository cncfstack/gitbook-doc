# 文本隐藏显示

## accordion 折叠模块

折叠模块, 别名叫手风琴。可以实现将内容隐藏起来，外部显示模块标题和显示箭头，点击箭头可显示里面的内容。

在文章中对主体内容进行细化说明较长，又不是文章核心逻辑时，可以隐藏细化说明部分，有需要时可以由读者决定是否阅读该部分细化说明。

### 开源地址


### 使用方法

在 books.json 中添加如下配置

```
{
  "plugins": ["accordion"]
}
```


在文章中使用标记对内容进行折叠

```
%accordion% 模块标题 %accordion%

- 内容部分第1行
- 内容部分第2行
- 内容部分第3行
- 内容部分第4行
- 内容部分第5行
- 内容部分第6行
- 内容部分第7行
- 内容部分第8行
- 内容部分第9行

%/accordion%
```


### 示例


%accordion% 模块标题 %accordion%

- 内容部分第1行
- 内容部分第2行
- 内容部分第3行
- 内容部分第4行
- 内容部分第5行
- 内容部分第6行
- 内容部分第7行
- 内容部分第8行
- 内容部分第9行

%/accordion%





## click-reveal

click-reveal 点击显示
默认隐藏，点击可显示。

### 使用方法

```
{
    "plugins": [
        "click-reveal"
    ]
}
```



使用方式，markdown中

```
{% reveal %}

要被隐藏的内容

{% endreveal %}
```


默认显示的文字是 Click to show , 使用自定义显示文字

```
{% reveal text="点击显示" %}

要被隐藏的内容

{% endreveal %}
```


支持html语法

```
<div class="click_reveal">
    <span> 点击显示 </span>
    <div><pre><code>隐藏的文字</code></pre></div>
</div>
```

### 示例



```
{% reveal text="点击显示" %}

要被隐藏的内容

{% endreveal %}
```


{% reveal text="点击显示>>>>" %}

- 要被隐藏的内容
- 要被隐藏的内容
- 要被隐藏的内容
- 要被隐藏的内容
- 要被隐藏的内容
- 要被隐藏的内容


{% endreveal %}




## hide-element

可以隐藏不想看到的元素，比如导航栏中 Published by GitBook

### 使用方法


```
{
    "plugins": [
        "hide-element"
    ],
    "pluginsConfig": {
        "hide-element": {
            "elements": [".gitbook-link"]
        }
    }
}
```

### 示例

默认可以移除最左侧下方的提示信息：

`Published with GitBook`

或

本书使用 Gitbook 发布

# sectionx

将页面分块显示
用于将页面分成多个部分，并添加按钮以允许读者控制每个部分的可见性。
下面有个更好的折叠模块功能accordion

```
{
    "plugins": [
           "sectionx"
    ]
}

```

## 使用方法

内容分块：

在 `.md` 文件中定义一个部分（就是插入下面的字段）。
markdown中示例代码：

```
<!--sec data-title="标题2" data-id="section0" data-show=true ces-->
内容部分2；
<!--endsec-->
```

这里只采用三个参数，其他参数如下所示：

参数	| 说明
-- | --
data-title	| 该部分的标题，它将显示为 bootstrap 面板的标题（大小为h2）。请注意，您不能使用"标题中的字符，请&quot;改用。
data-id	| 章节的id，对按钮控制很有用（在下一节中讨论）。
data-show	| 默认表示面板内容是否可见的布尔值。true：默认情况下，面板内容对用户可见，面板标题可以单击。false：默认情况下，面板内容对用户隐藏，面板标题不可点击，只能通过添加自定义按钮查看（在下一节中讨论）。
data-nopdf	| 一个布尔值，表示该部分是否将隐藏在 pdf 导出中。true：面板不会显示在.pdf或.epub中。
data-collapse	| 一个布尔值，表示默认情况下是否打开（但仍然可见）该部分。true：默认情况下，面板内容对用户可见，但已关闭。false：默认情况下，面板内容对用户可见，但已打开（默认设置）。

## 添加按钮，控制部分可见性

通过在 GitBook 中添加内联HTML，以下代码可以添加一个按钮，以允许您查看或隐藏其他部分。
简单来说，就是在【使用1】的内容部分添加一个按钮：

```
<button class="section" target="section1" show="显示下一部分" hide="隐藏下一部分"></button>
```

标签说明：

标签	| 说明
-- | --
class	| 该按钮必须属于类“section”。//这里就是用到 1部分的section
target	| 当按下时，将切换id为target的部分。
show	| 隐藏目标部分时按钮上的文本。
hide	| 目标部分可见时按钮上的文本。

markdown中示例代码：

```
<button class="section" target="section2" show="显示模块2" hide="隐藏模块2"></button>
<!--sec data-title="模块2" data-id="section2" data-show=true ces-->
内容部分2
<!--endsec-->
```

效果图2：

## 混合使用

将第2节的button块添加到第1节的内容部分
markdown中示例代码：

```
<!--sec data-title="标题1" data-id="section0" data-show=true ces-->
内容部分1；
<button class="section" target="section1" show="显示下一部分" hide="隐藏下一部分"></button>
<!--endsec-->
<!--sec data-title="标题2" data-id="section1" data-show=true ces-->
内容部分2
<!--endsec-->
```

效果图3：

<!--sec data-title="标题1" data-id="section0" data-show=true ces-->
内容部分1；
<button class="section" target="section1" show="显示下一部分" hide="隐藏下一部分"></button>
<!--endsec-->
<!--sec data-title="标题2" data-id="section1" data-show=true ces-->
内容部分2
<!--endsec-->



