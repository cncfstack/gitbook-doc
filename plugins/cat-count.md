# 访问统计

书籍站点的访问情况统计分析插件能够为站点管理人员提供数据参考，便于分析站点的访问量或相关数据。


## Google 统计
```
"plugins": [
    "ga"
 ],
"pluginsConfig": {
    "ga": {
        "token": "UA-XXXX-Y"
    }
}
```

## Baidu 统计

### 百度统计插件配置方法

```
{
    "plugins": ["3-ba"],
    "pluginsConfig": {
        "3-ba": {
            "token": "xxxxxxxx"
        }
    }
}
```

获取 Token

![baidu-3-ba-token](/assets/imgs/baidu-3-ba-token.png)

### 3-ba 统计效果




## pageview-count 页面阅读量计数

能够将页面访问的次数累计计数，页面每次刷新都会统计一次。

### 使用方法

```
{
  "plugins": [ "pageview-count"]
}
```

国内比较慢，还可能会引起页面布局异常

### 示例

![pageview-count](/assets/imgs/pageview-count.png)

