# Gitbook

Gitbook 是一款优秀的开源文档管理工具, 具有丰富的开源插件，它的源码可以在 GitHub 上获取。

GitBook 是基于 Node.js 的开源命令行工具，用于输出漂亮的电子书。

GitBook 支持 Markdown 和 AsciiDoc 两种语法格式，能够输出 html，pdf，epub，mobi或者输出为静态网页多种格式。

GitBook 特性
- Markdown 或 AsciiDoc 语法
- 多类型支持：网站(html)或电子书 (pdf, epub, mobi)
- 多语言
- 目录、大纲
- 封面
- 模板和变量
- 模板继承
- 插件
- 主题


## 缺陷
- 开源项目停止维护
活跃的开源项目，意味着不断完善的功能，不断修复bug，及时的反馈，更多社区的帮助。遗憾的是，GitBook开源项目已经停止维护，专注打造的 gitbook.com 网站在国内访问受限。

- 移动端不适配
现在有PC端的流量已经赶不上移动端了，但是 GitBook 对移动设备的支持不太友好。虽然正文部分做到了 responsive，但是菜单栏没有做移动端适配。大部分插件也没有考虑移动端的情形。


## 贡献文档
你可以贡献自己的一份力量来完善这个托管在 [Gitee](https://gitee.com/cncfstack/gitbook-doc/) 上的文档。


## 参考
- <https://chrisniael.gitbooks.io/gitbook-documentation/content/format/languages.html>
- <https://docs.gitbook.com/>
- <https://www.mapull.com/gitbook/comscore/basic/install.html>
- http://gitbook.zhangjikai.com/themes.html
- https://www.cnblogs.com/mingyue5826/p/10307051.html
- http://gitbook.zhangjikai.com/plugins.html#favicon
- https://www.cnblogs.com/mingyue5826/p/10307051.html
- <http://unclealan.cn/index.php/front/153.html>
