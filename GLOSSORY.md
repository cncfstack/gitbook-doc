# Gitbook
Gitbook 是一款优秀的开源文档管理工具, 具有丰富的开源插件，它的源码可以在 GitHub 上获取。Gitbook 使用 Git 和 Markdown 来构建书籍的工具。它可以将你的书输出很多格式：PDF，ePub，mobi，或者输出为静态网页。

# gitbook.com
gitbook.com 是一个使用 Gitbook 来创建和托管书籍的在线平台 (<www.gitbook.com>)。这里从互联网上整理了一些 Gitbook 相关的资料进行分类；同时，对于一些遇到的问题和经验也进行了记录。
