# 配置

所有的配置都以 JSON 格式存储在名为 `book.json` 的文件中。

没有这个文件，也能正常出书。如果你需要个性化添加一些功能，就需要它来配置各种参数。

如果需要这个文件来配置参数，需要手动创建一个 book.json 文件。 它必须是标准的 json 文件，格式错误将导致出书失败。


 字段

## gitbook

这个选项是用来指定生成书本的GitBook的版本的。

```
"gitbook" : "3.2.2",
"gitbook" : ">=3.0.0"
```

```
{ "gitbook": ">=2.0.0" }
```

这个选项是用来探测用来生成书本的 GitBook 的版本的。格式是一个 [SEMVER](https://www.zhaowenyu.com/softver/) 条件。

在 gitbook.com 中，这个值是根据平台中输入的标题定义的。


## title

设置书本的标题
```
"title" : "Gitbook 使用"
```
## author
作者的相关信息
```
"author" : "mapull"
```

## description

```
{ "description": "This is such a great book!" }
```
本书的简单描述，默认是从 README（第一段）中提取的。

"description" : "码谱整理的Gitbook的配置和使用"

这个选项定义了书本的描述，默认是从 README（第一段）中提取的。

在 gitbook.com 中，这个值是根据平台输入的描述定义的。

## isbn

```
{ "isbn": "978-3-16-148410-0" }
```

这个选项定义了你书本的ISBN。


## language

这个选项定义了你书本的语言，默认值是 en。

这个值是用来做国际化和本地化的，它改变网站的文字。

在 gitbook.com 中，这个值是根据探测到的语言或指定的设置定义的。

## direction

```
{ "direction": "rtl" }
```

这个选项是用来重新设置语言的文字方向的。建议将 language 字段设置为带有正确的文字方向的语言。


Gitbook使用的语言, 版本2.6.4中可选的语言如下：

```
en, ar, bn, cs, de, en, es, fa, fi, fr, he, it, ja, ko, no, pl, pt, ro, ru, sv, uk, vi, zh-hans, zh-tw
```

配置使用简体中文
```
"language" : "zh-hans"
```



## root

指定存放 GitBook 文件（除了 book.json）的根目录

```
"root": "."
```

或

```
"root": "./docs"
```




## links

在左侧导航栏添加链接信息

```
book.json

{"links" : {
    "sidebar" : {
        "首页" : "https://www.cncfstack.com"
    }
}}
```


## styles

自定义页面样式， 默认情况下各generator对应的css文件

```
{"styles": {
    "website": "styles/website.css",
    "ebook": "styles/ebook.css",
    "pdf": "styles/pdf.css",
    "mobi": "styles/mobi.css",
    "epub": "styles/epub.css"
}}
```

例如使 `<h1> <h2>` 标签有下边框， 可以在 website.css 中设置

```
website.css
h1 , h2{
    border-bottom: 1px solid #EFEAEA;
}
```



## plugins

配置使用的插件


book.json
```
{
"plugins": [
    "github"
]}
```

添加新插件之后需要运行 gitbook install 来安装新的插件

Gitbook默认带有7个插件：

- livereload 热加载插件
- highlight 语法高亮插件
- search 搜索插件
- lunr 搜索插件后台服务
- sharing 分享插件
- fontsettings 字体设置插件
- theme-default 主题

如果要去除自带的插件， 可以在插件名称前面加 - 移除搜索 search 插件：

```
{
"plugins": [
    "-search"
]}
```

## pluginsConfig
配置插件的属性

```
{"pluginsConfig": {
    "fontsettings": {
        "theme": "sepia",
        "family": "serif",
        "size":  1
    }
}}
```






## structure

这个选项是用来覆盖 GitBook 使用的路径的。

指定 Readme、Summary、Glossary 和 Languages 对应的文件名，下面是这几个文件对应变量以及默认值：

变量|	含义|	默认值
---|---|---
structure.readme|	Readme file name	|README.md
structure.summary|	Summary file name	|SUMMARY.md
structure.glossary|	Glossary file name	|GLOSSARY.md
structure.languages|	Languages file name	|LANGS.md


例如你想要使用 INTRO.md 代替 README.md：

```
{
    "structure": {
        "readme": "INTRO.md"
    }
}
```



## variables
```
{
    "variables": {
        "firstbook": "Hello World"
    }
}
```
这个选项定义在 模板 中使用的变量值。






