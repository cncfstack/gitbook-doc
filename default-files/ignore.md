# 忽略文件和文件夹

GitBook会读取 `.gitignore`，`.bookignore`，`.ignore` 文件来获取忽略的文件和目录的列表。

这些文件的格式，遵循和 	`.gitignore` 同样的约定：

```
# 这是一个注释

# 忽略文件test.md
test.md

# 忽略文件夹 "bin" 中的所有内容
bin/*
```


忽略文件和文件夹
GitBook 读取 .gitignore, .bookignore 和 .ignore 得到需要忽略的文件/文件夹的列表. (文件的格式和 .gitignore 一样).

.gitignore最佳实践是忽略 build文件，这些文件来自 node.js (node_modules, …) 和 GitBook 的 build 文件:_book, *.epub, *.mobi and *.pdf.