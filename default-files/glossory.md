# 词汇表 (GLOSSARY.md)

- GLOSSARY.md

允许你列出条目以及它们的定义. 基于这些条目 gitbook 会自动创建一个索引，并在页面中加亮这些条目.

格式 :

```
# term
Definition for this term
# Another term
With it's definition, this can contain bold text and all other kinds of inline markup ...
```