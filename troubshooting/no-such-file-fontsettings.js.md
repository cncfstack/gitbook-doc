#  no-such-file-fontsettings.js

## error 
```
# gitbook build .


Error: ENOENT: no such file or directory, stat '/cncfstack/linux-doc/_book/gitbook/gitbook-plugin-fontsettings/fontsettings.js'
```


## 定位及修复

文件路径

```
/root/.gitbook/versions/3.2.0/lib/output/website/copyPluginAssets.js
```


```
    return fs.copyDir(
        assetsFolder,
        assetOutputFolder,
        {
            deleteFirst: false,
            overwrite: true,
            confirm: true
        }
    );
```

将 `confirm: true` 修改为 `confirm: false`


## 使用 Docker 容器时修改

由于 gitbook 的 Docker 容器中没有 VIM 命令，可以将文件复制出来后修改在还原回去

```
docker cp gitbook:/root/.gitbook/versions/3.2.0/lib/output/website/copyPluginAssets.js ./copyPluginAssets.js
vim copyPluginAssets.js
docker cp copyPluginAssets.js gitbook:/root/.gitbook/versions/3.2.0/lib/output/website/copyPluginAssets.js
```

## 根因

- https://github.com/GitbookIO/gitbook/issues/1309#issuecomment-273584516
