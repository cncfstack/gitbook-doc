# Markdown

Gitbook 推荐使用 Markdown 的语法来组织文章内容。

Markdown 的使用方法, 在这本书中有详细的介绍：

- <https://www.zhaowenyu.com/markdown-doc/>
