# 输出格式

GitBook可以把你的书本生成为不同格式的电子书。

-  静态网站: 这是默认的格式。它生成一个可交互的静态站点, 主要由 HTML、Javascript、CSS 组成。
- PDF (Portable Document Format): 是以一种独立于软硬件，以及操作系统的方式来保存文档的格式。这是一种很普遍的格式。文件拥有的扩展名为 `.pdf`。
- ePub (electrontic publication): 是一个由国际电子出版物论坛 (IDPF) 制定的免费并开放的电子书标准。文件拥有的扩展名为 `.epub`，苹果和谷歌的设备支持 ePub 格式。
- Mobi (Mobipocket): Mobipocket 电子书格式是基于使用 XHTML 的开放电子书标准，并且可以包含 JavaScript 以及框架。亚马逊的设备 (Kindle) 支持这样的格式。
