# GitBook 快速上手


Gitbook 是一款优秀的 基于 Node.js 的开源文档管理工具，具有丰富的开源插件。

GitBook 支持 Markdown 和 AsciiDoc 两种语法格式，能够输出 html，pdf，epub，mobi 等多种格式。


## 安装与运行

Gitbook 是一款命令行工具，能够输出 html，pdf，epub，mobi 等多种格式文件。虽然也提供了 WebServer 的功能，但是由于其定位不是 Web 服务器，因此在性能上和 Nginx 等 Web 服务器差距还是挺大的。因此，一般通过 gitbook 的工具来输出 HTML 的静态页面，由 Nginx 来提供 Web 服务器功能。


在安装 Gitbook 时，可以在操作系统上依次安装 Nodejs，然后由 npm 来安装 gitbook-cli

```
npm install -g gitbook-cli
```

除了使用 npm 安装外，也可以通过 Docker 容器来运行。

```
docker run -itd --name gitbook fellah/gitbook bash
```


##  构建一本书

GitBook 可以设置一个样板书：

```
$ gitbook init
```

如果您希望将书籍创建到一个新目录中，可以通过运行 `gitbook init ./directory` 这样做。

在完成内容的编辑后，使用下面的命令，会在项目的目录下生成一个 _book 目录，里面的内容为静态站点的资源文件：

```
$ gitbook build
```

您可以使用选项 `--log=debug` 和 `--debug` 来获取更好的错误消息（使用堆栈跟踪）。例如：

```
$ gitbook build ./ --log=debug --debug
```

使用下列命令会运行一个 web 服务, 通过 `http://localhost:4000/` 可以预览书籍

```
$ gitbook serve
```

Gitbook 的书本可以输出为 html，pdf，epub，mobi 等格式，其中对于生成的静态页面，可以托管到 Github 或者 Gitbook 平台，通过绑定自定义的域名对外提供服务。

在国内访问 Github 和 Gitbook 的速度都比较慢，可以自行搭建 Nginx 类型 Web 服务器来展示输出的 HTLM 静态页面。

> [!TIP|style:flat]
>
> 云厂商的对象存储一般也会提供静态页面托管的功能(例如：阿里云)，可以将 Gitbook 渲染的 HTML 在云上托管。
>

## 内容组成

Gitbook 内容支持 Markdown 和 AsciiDoc 两种语法格式，同时在文章和文档编排中可以通过插件的来扩展功能。

除编辑的内容外，一个工程一般还会包含以下内容：

- SUMMARY.md
- book.json
- README.md
- GLOSSORY.md
- .gitignore

### SUMMARY.md

GitBook 使用文件 `SUMMARY.md` 来定义书本的章节和子章节的结构。`SUMMARY.md` 的格式是一个简单的链接列表，链接的名字是章节的名字，链接的指向是章节文件的路径。子章节被简单的定义为一个内嵌于父章节的列表。以 `#` 开头的行为注释被忽略。

```
# Summary
* [Introduction](README.md)
* [Part I](part1/README.md)
    * [Writing is nice](part1/writing.md)
    * [GitBook is nice](part1/gitbook.md)
* [Part II](part2/README.md)
    * [We love feedback](part2/feedback_please.md)
    * [Better tools for authors](part2/better_tools.md)
```

如果新项目文件是少量的，手动编写 SUMMARY.md 文件当然不费事。但是当文件数量非常多时，也可以通过插件来自动生成这些内容。

### book.json

Gitbook 所有的配置都以 JSON 格式存储在名为 `book.json` 的文件中。

没有这个文件，也能正常出书。如果你需要个性化添加一些功能，就需要它来配置各种参数。

如果需要这个文件来配置参数，需要手动创建一个 `book.json` 文件。 它必须是标准的 json 文件，格式错误将导致出书失败。

```
{
  "gitbook": ">=3.2.0",
  "description": "Gitbook 文档",
  "language": "zh-hans",
  "author": "温玉 <admin@zhaowenyu.com>",
  "generator": "site",
  "title": "Gitbook 文档",
  "variables": {
  },
  "links" : {
    "sidebar": {
        "CNCFSTACK": "https://www.zhaowenyu.com"
    }
  },
  "structure" : {
        "readme" : "README.md"
    },
  "plugins": [
    "sidebar-style",
  ],
  "pluginsConfig": {
    "sidebar-style": {
        "title": "Gitbook 文档",
        "author": "温玉"
    }
  }
}
```

### README.md

书本的首页内容默认是从文件 `README.md` 中提取的。`README.md` 是 gitbook 最基础的文件之一，它一般用来描述这本书最基本的信息。 它呈现给读者这本书最初的样子，如果内容不够简洁明了，很可能就没有看下去的欲望了。

一些项目更愿意将 README.md 文件作为项目的介绍而不是书的介绍。大部分代码托管平台将 `README.md` 自动显示到项目首页，如果你不喜欢这样。 从 `GitBook >2.0.0` 起，就可以在 `book.json` 中定义某个文件作为 README。

```
{
    "structure" : {
        "readme" : "information.md"
    }
}
```

### GLOSSORY.md

允许你指定术语并且在术语表中显示它们各自的定义。基于这些术语，GitBook 会自动建立索引并高亮这些在文中的术语。

`GLOSSORY.md` 的格式非常简单：

```
# 术语
这个术语的定义

# 另外一个术语
它的定义可以包含粗体和其他所有类型的内嵌式标记...
```

### .gitignore

GitBook会读取 `.gitignore`，`.bookignore`，`.ignore` 文件来获取忽略的文件和目录的列表。

这些文件的格式，遵循和 	`.gitignore` 同样的约定：

```
# 这是一个注释

# 忽略文件test.md
test.md

# 忽略文件夹 "bin" 中的所有内容
bin/*
```

## 插件

Gitbook 丰富的扩展插件是使其流程的核心亮点之一，Gitbook 默认带有7个插件（功能性5个，搜索有两个，主题一个）：

- livereload 热加载插件
- highlight 语法高亮插件
- search 搜索插件
- lunr 搜索插件后台服务
- sharing 分享插件
- fontsettings 字体设置插件
- theme-default 主题

除此之外，开发可以按照自己的需求定义自己的插件，开放性也导致了同一种的功能可能会有多个不同人员开发的插件，可以根据自己的喜好来选择插件。

侧边导航

Gitbook 提供的书籍默认都会在左侧提供书籍目录索引导航的功能。对于侧边导航功能、UI、布局、效果等等有一些开源的插件可供选择，插件的核心功能与差异：

- chapter-fold 插件: 默认的侧边目录是全部展开的，该插件可以让文章按照层级目录折叠起来，同时只能展开一个目录。
- expandable-chapters 插件: 默认的侧边目录是全部展开的，该插件可以让文章按照层级目录折叠起来，展开后不会自动折叠。
- expandable-chapters-small 插件: 默认的侧边目录是全部展开的，该插件可以让文章按照层级目录折叠起来，展开后不会自动折叠，箭头相比 expandable-chapters 会细一些。
- sidebar-style 插件: 会替换掉 Published by GitBook，并在左侧最上面显示标题。
- splitter 插件: 提供侧边栏宽度可调节功能。

文章目录导航:
- anchor-navigation-ex 插件
- ancre-navigation 插件
- back-to-top-button 插件
- page-toc-button 插件

文章 TOC 目录：
- page-treeview 插件
- page-treeview-simple 插件
- simple-page-toc 插件
- page-toc 插件

提示与强调：
- alerts 插件
- flexible-alerts 插件
- emphasize 插件

代码块：
- code 插件
- copy-code-button
- klipse
- Prism 语法高亮
- ACE
- Terminal

分享：
- sharing 插件
- sharing-plus 插件

打赏插件：
- donate 打赏插件
- reward 底部打上

查询：
- search
- search-pro 高级搜索
- search-plus 插件

访问统计：
- Google 统计
- Baidu 统计
- 3-ba 统计效果
- pageview-count 页面阅读量计数

站点版权：
- tbfed-pagefooter 插件
- page-copyright 插件

图片显示
- lightbox 插件
- popup 插件

图标与 LOGO
- custom-favicon 插件
- favicon
- insert-logo

文本显示与隐藏
- accordion 折叠模块
- click-reveal
- hide-element


图与图表
- 流程图（mermaid）
- Mermaid-gb3
- Puml
- Graph
- Chart
- Musicxml

其他还有很多优秀的插件，可以网站整理的插件列表中查看